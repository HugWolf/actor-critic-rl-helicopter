import numpy as np
import sklearn
from numpy import matlib
from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3

class q_helicopter(Agent):

    __state = []
    __prv_state = []

    __action = 0
    __smpl_action = 0
    __value = 0
    __prv_value = 0
    __discount = 0

    __sigma = 0.1
    __mean = [0, 0, 0, 0]
    __covariance = __sigma*np.identity(4)

    __max_step = 0
    __step = 0
    __n_layers = 5
    __input_layer_actor = 12
    __hidden_layer_actor = 12
    __output_layer_actor = 4
    __Psi_1 = __Psi_2 = __Psi_3 = __Psi_4 = []
    __a1_actor = __a2_actor = __a3_actor = __a4_actor = 0
    __z2_actor = __z3_actor = __z4_actor = 0
    __beta = 0.01
    __input_layer_critic = 12
    __hidden_layer_critic = 12
    __output_layer_critic = 1
    __Theta_1 = __Theta_2 = __Theta_3 = __Theta_4 = []
    __a1_critic = __a2_critic = __a3_critic = __a4_critic = 0
    __z2_critic = __z3_critic = __z4_critic = 0
    __alpha = 0.01


    def agent_init(self, taskSpecString):
        print "Task Specification String = \n", taskSpecString
        taskParser = TaskSpecVRLGLUE3.TaskSpecParser(taskSpecString)
        if taskParser.valid:
            self.__state = matlib.zeros((1, self.__input_layer_critic))
            self.__prv_state = matlib.zeros((1, self.__input_layer_critic))

            self.__discount = taskParser.getDiscountFactor()

            self.__a1_critic = matlib.ones((1, self.__input_layer_critic+1))
            self.__a2_critic = matlib.ones((1, self.__hidden_layer_critic+1))
            self.__a3_critic = matlib.ones((1, self.__hidden_layer_critic+1))
            self.__a4_critic = matlib.ones((1, self.__hidden_layer_critic+1))
            self.__Theta_1 = self.__sigma*matlib.randn((self.__hidden_layer_critic, self.__input_layer_critic+1))
            self.__Theta_2 = self.__sigma*matlib.randn((self.__output_layer_critic, self.__hidden_layer_critic+1))
            self.__Theta_3 = self.__sigma*matlib.randn((self.__hidden_layer_critic, self.__hidden_layer_critic+1))
            self.__Theta_4 = self.__sigma*matlib.randn((self.__output_layer_critic, self.__hidden_layer_critic+1))

            self.__a1_actor = matlib.ones((1, self.__input_layer_actor+1))
            self.__a2_actor = matlib.ones((1, self.__hidden_layer_actor+1))
            self.__a3_actor = matlib.ones((1, self.__hidden_layer_actor+1))
            self.__a4_actor = matlib.ones((1, self.__hidden_layer_actor+1))
            self.__Psi_1 = self.__sigma*matlib.randn((self.__hidden_layer_actor, self.__input_layer_actor+1))
            self.__Psi_2 = self.__sigma*matlib.randn((self.__output_layer_actor, self.__hidden_layer_actor+1))
            self.__Psi_3 = self.__sigma*matlib.randn((self.__hidden_layer_actor, self.__hidden_layer_actor+1))
            self.__Psi_4 = self.__sigma*matlib.randn((self.__output_layer_actor, self.__hidden_layer_actor+1))

    def agent_start(self, observation):
        print "[START]"
        self.__step += 1
        print "Observations =\n", observation.doubleArray
        #self.NNcycle(observation)
        self.__state = self.fromOtoState(observation)
        self.__action = self.feedForward(self.__state, "actor")
        print "FF Action = \n", self.__action.A
        self.__smpl_action = (self.__action.A.reshape((4,)) + np.random.multivariate_normal(self.__mean, self.__covariance)).tolist()
        act = Action(None, 4, None)
        act.doubleArray = self.__smpl_action
        print "Act = \n", act.doubleArray
        self.__value = self.feedForward(self.__state, "critic") #.reshape((1,))).tolist()
        print "Value = \n", self.__value
        return act

    def agent_step(self, reward, observation):
        self.__step += 1
        print "Observations =\n", observation.doubleArray
        print "Reward = ", reward
        self.NNcycle(observation, reward)
        act = Action(None, 4, None)
        self.__smpl_action
        act.doubleArray = self.__smpl_action
        print "Act = \n", act.doubleArray
        if np.isnan(self.__value): 
            print "VALUE IS NAN!!!"
            return
        else:
            print "Value = ", self.__value 
            return act

    def agent_end(self, reward):
        print "Reward = ", reward
        td_target = reward
        td_error = reward - self.__prv_value
        self.backPropagation(np.asmatrix(td_target), "critic")
        self.__value = 0
        self.__prv_value = 0
        if (self.__step > self.__max_step): self.__max_step= self.__step 
        self.__step = 0


    def agent_cleanup(self):
        print "Max Execution Step = ", self.__max_step

    def agent_message(self, inMessage):
        pass

    def fromOtoState(self, observation):
        state = matlib.zeros((1, self.__input_layer_critic))
        state[:, 0:3] = observation.doubleArray[3:6] # Position
        state[:, 6:9] = observation.doubleArray[0:3] # Velocities
        state[:, 9:] = observation.doubleArray[6:9] # Angular Rate

        # Compute angles from quaternion
        q_xyz = observation.doubleArray[9:]
        qq = np.dot(q_xyz, q_xyz)
        q_w = np.sqrt(1 - qq)
        Q = np.insert(q_xyz, 0, q_w)
        euler_angles = self.fromQtoEuler(Q)
        state[:, 3:6] = euler_angles
        print state
        return state

    def fromQtoEuler(self, Q):
        rpy = np.empty((1, 3))
        Qy_sqr = Q[2]*Q[2]
        rpy[:, 0] = np.arctan2( 2*((Q[0]*Q[1])+(Q[2]*Q[3])), 1-2*((Q[1]*Q[1])+Qy_sqr) )
        pitch = np.arcsin(2*((Q[0]*Q[2])-(Q[3]*Q[1])))
        rpy[:, 1] = 1.0 if (pitch>1.0) else pitch
        rpy[:, 1] = -1.0 if (pitch<-1.0) else pitch
        rpy[:, 2] = np.arctan2( 2*((Q[0]*Q[3])+(Q[2]*Q[1])), 1-2*((Q[3]*Q[3])+Qy_sqr) )
        return rpy

    def lazyReLU(self, state):
        return np.maximum(state, 0)

    def invertedLazyReLU(self, state):
        return np.minimum(state, 0)

    def lazyReLUGrad(self, state):
        return (state>0).astype(float)

    def tanhGrad(self, state):
        return np.asmatrix(1-np.square(np.tanh(state)))

    def softmax(self, state):
        return np.exp(state) / np.sum(np.exp(state))

    def backPropagation(self, target, actor_critic):
        if actor_critic=="actor":
            '''Delta4 = np.multiply(error*self.__Psi_4[:,1:], self.tanhGrad(self.__z4_actor))
            Delta3 = np.multiply(Delta4*self.__Psi_3[:,1:], self.tanhGrad(self.__z3_actor))
            Delta2 = np.multiply(Delta3*self.__Psi_2[:,1:], self.tanhGrad(self.__z2_actor))'''

            Delta2 = np.multiply(target*self.__Psi_2[:,1:], self.lazyReLUGrad(self.__z2_actor))

            '''print "[BP] Psi4 BEFORE \n", self.__Psi_4
            self.__Psi_4 += self.__beta*(error.T*self.__a4_actor)
            print "[BP] Psi4 AFTER \n", self.__Psi_4
            self.__Psi_3 += self.__beta*(Delta4.T*self.__a3_actor)
            self.__Psi_2 += self.__beta*(Delta3.T*self.__a2_actor)
            self.__Psi_1 += self.__beta*(Delta2.T*self.__a1_actor)'''

            self.__Psi_2 -= self.__beta*(target.T*self.__a2_actor)
            self.__Psi_1 -= self.__beta*(Delta2.T*self.__a1_actor)

        elif actor_critic=="critic":
            '''Delta4 = np.multiply(error*self.__Theta_4[:,1:], self.tanhGrad(self.__z4_critic))
            print "Delta4 is ",Delta4.shape
            Delta3 = np.multiply(Delta4*self.__Theta_3[:,1:], self.tanhGrad(self.__z3_critic))
            print "Delta3 is ",Delta3.shape
            Delta2 = np.multiply(Delta3*self.__Theta_2[:,1:], self.tanhGrad(self.__z2_critic))
            print "Delta2 is ",Delta2.shape'''

            error = self.__prv_value - target
            Delta2 = np.multiply(error*self.__Theta_2[:,1:], self.lazyReLUGrad(self.__z2_critic))
            print "Delta2 is ",Delta2.shape

            self.__Theta_2 -= self.__alpha*(error.T*self.__a2_critic)
            self.__Theta_1 -= self.__alpha*(Delta2.T*self.__a1_critic)

    def feedForward(self, state, actor_critic):
        output = 0

        if actor_critic=="actor":
            '''# 1 LV
            self.__a1_actor[:, 1:] = state

            # 2 LV
            self.__z2_actor = self.__a1_actor*self.__Psi_1.T
            self.__a2_actor[:, 1:] = np.tanh(self.__z2_actor)

            # 3 LV
            self.__z3_actor = self.__a2_actor*self.__Psi_2.T
            self.__a3_actor[:, 1:] = np.tanh(self.__z3_actor)

            # 4 LV
            self.__z4_actor = self.__a3_actor*self.__Psi_3.T
            self.__a4_actor[:, 1:] = np.tanh(self.__z4_actor)

            # 5 LV
            self.__z5_actor = self.__a4_actor*self.__Psi_4.T
            output = np.tanh(self.__z5_actor)'''

            # 1 LV
            self.__a1_actor[:, 1:] = state

            # 2 LV
            self.__z2_actor = self.__a1_actor*self.__Psi_1.T
            self.__a2_actor[:, 1:] = self.lazyReLU(self.__z2_actor)

            # 3 LV
            self.__z3_actor = self.__a2_actor*self.__Psi_2.T
            '''self.__a3_actor[:, 1:] = self.lazyReLU(self.__z3_actor)

            # 4 LV
            self.__z4_actor = self.__a3_actor*self.__Psi_3.T
            self.__a4_actor[:, 1:] = self.lazyReLU(self.__z4_actor)

            # 5 LV
            self.__z5_actor = self.__a4_actor*self.__Psi_4.T'''
            output = self.__z3_actor

        elif actor_critic=="critic":
            '''# 1 LV
            self.__a1_critic[:, 1:] = state

            # 2 LV
            self.__z2_critic = self.__a1_critic*self.__Theta_1.T
            self.__a2_critic[:, 1:] = np.tanh(self.__z2_critic)

            # 3 LV
            self.__z3_critic = self.__a2_critic*self.__Theta_2.T
            self.__a3_critic[:, 1:] = np.tanh(self.__z3_critic)

            # 4 LV
            self.__z4_critic = self.__a3_critic*self.__Theta_3.T
            self.__a4_critic[:, 1:] = np.tanh(self.__z4_critic)

            # 5 LV
            self.__z5_critic = self.__a4_critic*self.__Theta_4.T
            output = np.tanh(self.__z5_critic)'''

            # 1 LV
            self.__a1_critic[:, 1:] = state

            # 2 LV
            self.__z2_critic = self.__a1_critic*self.__Theta_1.T
            self.__a2_critic[:, 1:] = self.lazyReLU(self.__z2_critic)

            # 3 LV
            self.__z3_critic = self.__a2_critic*self.__Theta_2.T
            '''self.__a3_critic[:, 1:] = self.lazyReLU(self.__z3_critic)

            # 4 LV
            self.__z4_critic = self.__a3_critic*self.__Theta_3.T
            self.__a4_critic[:, 1:] = self.lazyReLU(self.__z4_critic)

            # 5 LV
            self.__z5_critic = self.__a4_critic*self.__Theta_4.T'''
            output = self.__z3_critic

        return output

    def NNcycle(self, observation, reward):
        
        self.__state = self.fromOtoState(observation)
        print "Value = ", self.__value
        self.__prv_value = self.__value
        print "Prv_Value = ", self.__prv_value
        self.__value = self.feedForward(self.__state, "critic")
        print "Value = ", self.__value
        td_target = reward + self.__discount*self.__value
        td_error = td_target - self.__prv_value
        self.backPropagation(np.asmatrix(td_error), "critic")
        if td_error>0 :
            print "TD-Error > 0"
            act_error = self.__action - self.__smpl_action
            self.backPropagation(np.asmatrix(act_error), "actor")
        self.__action = self.feedForward(self.__state, "actor")
        #self.__action[:, 0:4] = -self.__action[:, 0:4]
        print "FF Action = \n", self.__action
        self.__smpl_action = (self.__action.A.reshape((4,)) + np.random.multivariate_normal(self.__mean, self.__covariance)).tolist()


if __name__ == "__main__":
    AgentLoader.loadAgent(q_helicopter())
