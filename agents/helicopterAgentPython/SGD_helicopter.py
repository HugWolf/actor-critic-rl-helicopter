import itertools
import numpy as np
import sklearn.pipeline
import sklearn.preprocessing
import warnings

from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3
from sklearn.linear_model import SGDRegressor
from sklearn.kernel_approximation import RBFSampler

warnings.filterwarnings('ignore')
global action_dim, state_dim, sigma, featurizer

action_dim = 4
state_dim = 12

sigma = 0.15

featurizer = sklearn.pipeline.FeatureUnion([
        ("rbf1", RBFSampler(gamma=5.0, n_components=3)),
        ("rbf2", RBFSampler(gamma=2.0, n_components=3)),
        ("rbf3", RBFSampler(gamma=1.0, n_components=3)),
        ("rbf4", RBFSampler(gamma=0.5, n_components=3))
        ])
def featurize_state(state):
    """
    Returns the featurized representation for a state.
    """
    featurized = featurizer.fit_transform(state)
    return featurized[0]

class Actor():

    def __init__(self):
        self.__action_dim = action_dim
        self.__state_dim = state_dim
        self.models_dims_action = []
        for dim_action in range(self.__action_dim):
            model = SGDRegressor()
            model.partial_fit(featurize_state(np.zeros(self.__state_dim)), np.reshape(np.random.uniform(low=-1.0, high=1.0),(1,-1)))
            print "Actor Model %d" % (dim_action)
            print "\tWeights\n", model.coef_
            print "\tIntercept\n", model.intercept_
            self.models_dims_action.append(model)

    def predict(self, s):
        features = featurize_state(s)
        Action = []
        for dim_action in range(self.__action_dim):
            dim_action
            Action.append(self.models_dims_action[dim_action].predict([features])[0])
        return np.array(Action)
    
    def update(self, s, y):
        features = featurize_state(s)
        for dim_action in range(self.__action_dim):
            self.models_dims_action[dim_action].partial_fit([features], np.reshape(y[dim_action],(1, -1)))

class Critic():

    def __init__(self):
        self.__state_dim = state_dim
        self.model = SGDRegressor()
        self.model.partial_fit(featurize_state(np.zeros(self.__state_dim)), np.reshape(0,(1,-1)))

    def predict(self, state):
        features = featurize_state(state)
        return self.model.predict([features])

    def update(self, state, target):
        features = featurize_state(state)
        if isinstance(target, float):
            self.model.partial_fit([features], np.reshape(target, (1,-1)))
        else:
            self.model.partial_fit([features], np.reshape(target,(1,-1)))


def make_gaussian_policy(estimator):

    def policy_fn(state):
        A = estimator.predict(state)
        return A, A + np.random.multivariate_normal([0., 0., 0., 0.], np.eye(4,4)*sigma)
    return policy_fn


class SGD_helicopter(Agent):

    __step = 0
    __max_step = 0
    __rollout = 0

    def agent_init(self, taskSpecString):
        print "Task Specification String = \n", taskSpecString
        taskParser = TaskSpecVRLGLUE3.TaskSpecParser(taskSpecString)
        if taskParser.valid:
            self.actor = Actor()
            self.critic = Critic()
            self.__state_dim = state_dim
            self.__value = 0
            self.__prv_value = 0
            self.__state = np.zeros(self.__state_dim)
            self.__prv_state = np.zeros(self.__state_dim)
            self.__discount = taskParser.getDiscountFactor()
            self.policy = make_gaussian_policy(self.actor)

    def agent_start(self, observation):
        self.__rollout += 1
        print "****************************"
        print "***                       **"
        print "**   ROLL OUT NUMBER %d   **************************************************************" % (self.__rollout)
        print "***                       **"
        print "****************************"
        print "Observations =\n", observation.doubleArray
        self.__step += 1
        self.__state = self.fromOtoState(observation)
        self.__value = self.critic.predict(self.__state)
        self.__action, self.__smpl_action = self.policy(self.__state)
        print "FF Action = \n", self.__action
        act = Action(None, 4, None)
        act.doubleArray = self.__smpl_action
        print "Act = \n", act.doubleArray
        return act

    def agent_step(self, reward, observation):
        print "Observations =\n", observation.doubleArray
        print "Reward = ", reward
        self.__step += 1
        self.__prv_state = self.__state
        self.__state = self.fromOtoState(observation)
        self.__prv_value = self.__value
        print "Prv_Value = ", self.__prv_value
        self.__value = self.critic.predict(self.__state)
        print "Value = ", self.__value
        td_target = reward + self.__discount*self.__value
        td_error = td_target - self.__prv_value
        self.critic.update(self.__prv_state, td_target)
        if np.abs(self.__value)<np.abs(self.__prv_value):
            print"***********\n[ TD > 0 ]\n***********"
            print "TD error = ", td_error
            act_error = self.__smpl_action - self.__action
            print "Act error = ", act_error
            self.actor.update(self.__prv_state, act_error)
            print "Action Current State = ", self.actor.predict(self.__prv_state)
        act = Action(None, 4, None)
        self.__action, self.__smpl_action = self.policy(self.__state)
        act.doubleArray = self.__smpl_action
        print "New Action = \n", act.doubleArray
        if np.isnan(self.__value): 
            print "VALUE IS NAN!!!"
            return
        else:
            return act

    def agent_end(self, reward):
        print "Reward = ", reward
        td_error = reward - self.__prv_value
        self.critic.update(self.__prv_state, reward)
        if np.abs(reward)<np.abs(self.__prv_value):
            print"***********\n[ TD > 0 ]\n***********"
            print "TD error = ", td_error
            act_error = self.__smpl_action - self.__action
            print "Act error = ", act_error
            self.actor.update(self.__prv_state, act_error)
        if (self.__step > self.__max_step): self.__max_step = self.__step 
        self.__step = 0
        self.__prv_value = 0
        self.__prv_state = np.zeros(self.__state_dim)
        print "**********************************"
        print "***                             **"
        print "**   [END] ROLL OUT NUMBER %d   **************************************************************" % (self.__rollout)
        print "***                             **"
        print "**********************************"


    def agent_cleanup(self):
        print "Max Execution Step = ", self.__max_step
        for dim_action in range(self.__action_dim):
            print "Actor Model %d" % (dim_action)
            print "\tWeights\n", self.actor.models_dims_action[dim_action].coef_
            print "\tIntercept\n", self.actor.models_dims_action[dim_action].intercept_

    def agent_message(self, inMessage):
        pass

    def fromOtoState(self, observation):
        state = np.zeros(self.__state_dim)
        state[0:3] = observation.doubleArray[3:6] # Position
        state[6:9] = observation.doubleArray[0:3] # Velocities
        state[9:] = observation.doubleArray[6:9] # Angular Rate

        # Compute angles from quaternion
        q_xyz = observation.doubleArray[9:]
        qq = np.dot(q_xyz, q_xyz)
        q_w = np.sqrt(1 - qq)
        Q = np.insert(q_xyz, 0, q_w)
        euler_angles = self.fromQtoEuler(Q)
        state[3:6] = euler_angles
        return state

    def fromQtoEuler(self, Q):
        rpy = np.empty(3)
        Qy_sqr = Q[2]*Q[2]
        rpy[0] = np.arctan2( 2*((Q[0]*Q[1])+(Q[2]*Q[3])), 1-2*((Q[1]*Q[1])+Qy_sqr) )
        pitch = np.arcsin(2*((Q[0]*Q[2])-(Q[3]*Q[1])))
        rpy[1] = 1.0 if (pitch>1.0) else pitch
        rpy[1] = -1.0 if (pitch<-1.0) else pitch
        rpy[2] = np.arctan2( 2*((Q[0]*Q[3])+(Q[2]*Q[1])), 1-2*((Q[3]*Q[3])+Qy_sqr) )
        return rpy


if __name__ == "__main__":
    AgentLoader.loadAgent(SGD_helicopter())