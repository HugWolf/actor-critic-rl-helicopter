from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3

agentName = "Weak Baseline Controller"
weights = [0.0196, 0.7475, 0.0367, 0.0185, 0.7904, 0.0322, 0.1969, 0.0513, 0.1348, 0.02, 0, 0.23]

u_err = 0   # forward velocity
v_err = 1   # sideways velocity
w_err = 2   # downward velocity
x_err = 3   # forward error
y_err = 4   # sideways error
z_err = 5   # downward error
p_err = 6   # angular rate around forward axis
q_err = 7   # angular rate around sideways (to the right) axis
r_err = 8   # angular rate around vertical (downward) axis
qx_err = 9  # <-- quaternion entries, x,y,z,w   q = [ sin(theta/2) * axis; cos(theta/2)],
qy_err = 10 # where axis = axis of rotation; theta is amount of rotation around that axis
qz_err = 11 # [recall: any rotation can be represented by a single rotation around some axis]

y_w = 0
roll_w = 1
v_w = 2
x_w = 3
pitch_w = 4
u_w = 5
yaw_w = 6
z_w = 7
w_w = 8
ail_trim = 9
el_trim = 10
coll_trim = 11

class weak_baseline_controller(Agent):

    def agent_init(self, taskSpecString):
        print "Task Specification String = \n", taskSpecString

    def agent_start(self, observation):
        print "[Agent STARTS]"
        print "Observations =\n", observation.doubleArray
        return self.agent_policy(observation)

    def agent_step(self, reward, observation):
        print "[STEP]"
        print "Observations =\n", observation.doubleArray
        print "Reward = ", reward
        return self.agent_policy(observation)

    def agent_end(self, reward):
        print "[TERMINAL STATE]"
        print "Reward = ", reward

    def agent_cleanup(self):
        pass

    def agent_message(self, inMessage):
        return agentName+" does not respond to any messages."

    def print_observation(self, observation):
        print "Observation = \n", observation

    def agent_policy(self, observation):
        action = Action(None, 4, None)
        action.doubleArray[0] = ((-weights[y_w] * observation.doubleArray[y_err]) + (-weights[v_w] * observation.doubleArray[v_err]) + 
                                (-weights[roll_w] * observation.doubleArray[qx_err]) + weights[ail_trim]) # left-right control
        action.doubleArray[1] = ((-weights[x_w] * observation.doubleArray[x_err]) + (-weights[u_w] * observation.doubleArray[u_err]) +
                                (weights[pitch_w] * observation.doubleArray[qy_err]) + weights[el_trim]) # forward-backward control
        action.doubleArray[2] = -weights[yaw_w] * observation.doubleArray[qz_err] # rudder
        action.doubleArray[3] = ((weights[z_w] * observation.doubleArray[z_err]) + (weights[w_w] * observation.doubleArray[w_err]) +
                                weights[coll_trim]) # collective control
        print "Action = \n", action.doubleArray
        return action

        

if __name__ == "__main__":
    AgentLoader.loadAgent(weak_baseline_controller())